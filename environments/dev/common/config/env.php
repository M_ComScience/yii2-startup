<?php
/**
 * Load application environment from .env file
 */
 (new \Dotenv\Dotenv(__DIR__ . '/../../'))->load();