<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\TablesFieldsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tables-fields-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'table_id') ?>

    <?= $form->field($model, 'table_name') ?>

    <?= $form->field($model, 'table_varname') ?>

    <?= $form->field($model, 'table_field_type') ?>

    <?= $form->field($model, 'table_length') ?>

    <?php // echo $form->field($model, 'table_default') ?>

    <?php // echo $form->field($model, 'table_index') ?>

    <?php // echo $form->field($model, 'input_field') ?>

    <?php // echo $form->field($model, 'input_label') ?>

    <?php // echo $form->field($model, 'input_hint') ?>

    <?php // echo $form->field($model, 'input_specific') ?>

    <?php // echo $form->field($model, 'input_data') ?>

    <?php // echo $form->field($model, 'input_required') ?>

    <?php // echo $form->field($model, 'input_validate') ?>

    <?php // echo $form->field($model, 'input_meta') ?>

    <?php // echo $form->field($model, 'input_order') ?>

    <?php // echo $form->field($model, 'action_id') ?>

    <?php // echo $form->field($model, 'begin_html') ?>

    <?php // echo $form->field($model, 'end_html') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
