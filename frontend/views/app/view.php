<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\TablesFields */

$this->title = $model->table_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tables-fields-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'table_id',
            'table_name',
            'table_varname',
            'table_field_type',
            'table_length',
            'table_default:ntext',
            'table_index',
            'input_field',
            'input_label',
            'input_hint:ntext',
            'input_specific:ntext',
            'input_data:ntext',
            'input_required',
            'input_validate:ntext',
            'input_meta:ntext',
            'input_order',
            'action_id',
            'begin_html:ntext',
            'end_html:ntext',
            'updated_at',
            'updated_by',
            'created_at',
            'created_by',
            'status',
        ],
    ]) ?>

</div>
