<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model msoft\core\models\TablesFields */

$this->title = Yii::t('app', 'Create Tables Fields');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tables-fields-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
