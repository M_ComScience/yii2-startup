<?php
use msoft\helpers\Html;
use msoft\core\classes\CoreQuery;
use msoft\core\classes\CoreFunc;
use msoft\widgets\ActiveForm;
use msoft\core\models\TablesFields;
use msoft\widgets\Panel;
$this->title = 'Form Example';
?>
<?php 
$attributes = CoreQuery::getAttributesForm('DemoForm');
$model = new TablesFields();

echo Panel::begin([
    'title'=>'Demo Form Auto Genarate',
    'type' => 'primary',
    'widget'=>false, // no js included (ie. style only)
	'footer'=>false,
	'collapse'=>false,
]);

$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);

if(isset($attributes)){
    echo CoreFunc::renderInput($model,$form,$attributes);
}
echo Html::beginTag('div',['class'=>'form-group']);
    echo Html::beginTag('div',['class' => 'col-sm-11','style' => 'text-align:right;']);
        echo Html::resetButton('Reset',['class' => 'btn btn-danger']).' '.Html::submitButton('Submit', ['class'=>'btn btn-primary']);
    echo Html::endTag('div');
echo Html::endTag('div');
ActiveForm::end();

Panel::end();
?>