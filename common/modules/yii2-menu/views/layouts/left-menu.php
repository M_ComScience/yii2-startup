<?php

use yii\helpers\Html;
use yii\helpers\BaseStringHelper;

/* @var $this \yii\web\View */
/* @var $content string */

$controller = $this->context;
//$menus = $controller->module->menus;
//$route = $controller->route;
?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>
<div class="row">
    <div class="col-sm-12">
        <?= $content ?>
    </div>
</div>
    
<?php $this->endContent(); ?>
