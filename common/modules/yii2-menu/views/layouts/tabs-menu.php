<?php
use msoft\widgets\Icon;
use yii\helpers\Url;
use msoft\widgets\TabsX;

echo TabsX::widget([
    'items'=>[
        [
            'label'=>'จัดการเมนู',
            'active'=> ($active == 'จัดการเมนู') ? true : false,
            'options' => ['id' => 'tabs1'],
            'url' => Url::to(['/menu/default/index'])
        ],
        [
            'label'=>'จัดเรียงเมนู',
            'active'=> ($active == 'จัดเรียงเมนู') ? true : false,
            'options' => ['id' => 'tabs2'],
            'url' => Url::to(['/menu/default/sort-menus'])
        ],
        [
            'label'=>'จัดการประเภทเมนู',
            'active'=> ($active == 'จัดการประเภทเมนู') ? true : false,
            'options' => ['id' => 'tabs3'],
            'url' => Url::to(['/menu/category/index'])
        ],
    ],
    'position'=>TabsX::POS_LEFT,
    'encodeLabels'=>false,
    'renderTabContent' => false
]);
?>