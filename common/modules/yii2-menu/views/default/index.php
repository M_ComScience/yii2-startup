<?php

use yii\helpers\Html;
use msoft\widgets\Datatables;
use msoft\menu\models\Menu;
use msoft\menu\models\MenuCategory;
use msoft\widgets\Icon;
use yii\helpers\Url;
use yii\widgets\Pjax;
use msoft\helpers\RegisterJS;
use msoft\widgets\swalalert\SwalAlert;
use msoft\widgets\Panel;

RegisterJS::regis(['sweetalert'],$this);

$this->title = Yii::t('menu', 'ระบบจัดการเมนู');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= SwalAlert::widget(); ?>
<div class="row">
    <div class="col-sm-2">
        <?php echo $this->render('../layouts/tabs-menu.php',['active' => 'จัดการเมนู']); ?>
    </div>
    <div class="col-sm-10">
        <div class="tab-content printable">
            <div id="tabs1" class="tab-pane fade active in">
                <?php echo Panel::begin([
                    'title'=> $this->title,
                    'type' => 'default',
                    'widget'=>false
                ]); ?>
                    <?php Pjax::begin(["id" => "crud-datatable-pjax"]); ?> 
                    <?=
                    Datatables::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'hover' => true,
                        'bordered' => false,
                        'condensed' => true,
                        'striped' => true,
                        'export' => false,
                        'clientOptions' =>[
                            'language' => [
                                "search" => "ค้นหา: _INPUT_". ' '. Html::a(Icon::show('plus').' Add', ['create'], ['class' => 'btn btn-primary','role'=>'modal-remote']).' '.
                                Html::a('ClearCache',false,['class' => 'btn btn-warning','onclick' => 'Menu.ClearCache(this);']),
                            ]
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'name',
                                'format'=>'raw',
                                'value' => function($model) {
                                    return Html::a($model->iconShow.' '.$model->title,['/menu/default/view','id'=>$model->id],['role' => 'modal-remote']);
                                }
                            ],
                            [
                                'attribute' => 'menu_category_id',
                                //'filter' => MenuCategory::getList(),
                                'value' => function($model) {
                                    return $model->menu_category_id?$model->menuCategory->title:null;
                                }
                            ],
                            [
                                'attribute' => 'route',
                                //'filter' => Menu::getRouterDistinct(),                 
                            ],
                            [
                                'attribute' => 'parent_id',
                                //'filter' => Menu::getParentDistinct(),
                                'value' => function($model) {
                                    return $model->parentTitle;
                                }
                            ],
                            // 'parameter',
                            // 'icon',
                            
                            [
                                'attribute' => 'status',
                                //'filter' => Menu::getItemStatus(),
                                'value' => 'statusLabel',
                            ],
                            //'item_name',                      
                            [
                                'attribute' => 'auth_items',
                                //'filter' => Menu::getItemsListDistinct(),
                                'value' => function ($model, $key, $index, $column){
                                    return !empty($model->auth_items) ?  implode(',',$model->auth_items) : '';
                                },
                                'headerOptions' => ['width' => '200']
                            ],
                            'sort',
                            [
                                'class' => 'msoft\menu\widgets\ActionColumn',
                                'contentOptions' => ['style' => 'text-align:center;'],
                                'viewOptions' => [
                                    'role' => 'modal-remote'
                                ],
                                'updateOptions' => [
                                    'role' => 'modal-remote'
                                ]
                            ],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?>
                <?php Panel::end(); ?>
            </div>
        </div>
    </div>
</div>


<?php echo $this->render('modal'); ?>

<?php 
$this->registerJs(<<<JS
Menu = {
    ClearCache : function(){
        $.ajax({
            method: "POST",
            url: "clearcache-menu",
            success: function (data) {
                swal(
                    'Success!',
                    '',
                    'success'
                );
            },
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        });
    }
};
JS
);?>