/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : yii2-startup

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-09-28 14:39:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `router` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `parameter` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('2','1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `item_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protocol` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `sort` int(3) DEFAULT NULL,
  `language` varchar(7) COLLATE utf8_unicode_ci DEFAULT '*',
  `params` mediumtext COLLATE utf8_unicode_ci,
  `assoc` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `auth_items` longtext COLLATE utf8_unicode_ci,
  `options` longtext COLLATE utf8_unicode_ci,
  `module_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_menu_category_id_5207_00` (`menu_category_id`),
  KEY `idx_parent_id_5207_01` (`parent_id`),
  CONSTRAINT `fk_menu_category_5187_00` FOREIGN KEY (`menu_category_id`) REFERENCES `menu_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '1', null, 'Dashboard', '/', '', 'home', '1', null, '', '', '1', '1', '*', '', null, '1506553427', '1', null, null, '', null, null, '[\"manageUsers\"]', '', '');
INSERT INTO `menu` VALUES ('2', '1', null, 'Settings', '#', '', 'cogs', '1', null, '', '', '1', '6', '*', '', null, '1506565780', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('4', '1', '2', 'Menus', '/menu/default/index', '', 'th-list', '1', null, '', '', '1', '7', '*', '', null, '1506567511', '1', null, null, '', null, null, '[\"Settings\"]', '', 'menu');
INSERT INTO `menu` VALUES ('5', '1', '2', 'Users', '/user/admin/index', '', 'users', '1', null, '', '', '1', '8', '*', '', null, '1506567670', '1', null, null, '', null, null, '[\"Settings\"]', '', 'user');
INSERT INTO `menu` VALUES ('6', '1', '2', 'Permission', '/admin/assignment', '', 'lock', '1', null, '', '', '1', '9', '*', '', null, '1506567817', '1', null, null, '', null, null, '[\"Settings\"]', '', 'admin');
INSERT INTO `menu` VALUES ('7', '1', null, 'Core Module', '/core', '', 'wrench', '1', null, '', '', '1', '5', '*', '', null, '1506572028', '1', null, null, '', null, null, '[\"Settings\"]', '', 'core');
INSERT INTO `menu` VALUES ('8', '1', null, 'Demo Form', '/site/example', '', 'tags', '1', null, '', '', '1', '3', '*', '', null, '1506573475', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('9', '1', null, 'Gii', '/gii', '', 'tags', '1', null, '', '', '1', '2', '*', '', null, '1506577241', '1', null, null, '', null, null, '[\"Settings\"]', '', 'gii');
INSERT INTO `menu` VALUES ('10', '1', null, 'Demo Ajax Crud', '/app/index', '', 'tags', '1', null, '', '', '1', '4', '*', '', null, '1506578901', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('11', '2', null, 'Dashboard', '/', '', 'home', '1', null, '', '', '1', '1', '*', '', null, '1506582130', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('12', '2', null, 'Gii', '/gii', '', 'tags', '1', null, '', '', '1', '2', '*', '', null, '1506582167', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('13', '1', null, 'Backend', '/app-backend', '', 'bookmark', '1', null, '', '', '1', '7', '*', '', null, '1506582589', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
INSERT INTO `menu` VALUES ('15', '2', null, 'Menus', '/menu/default/index', '', 'th-list', '1', null, '', '', '1', '3', '*', '', null, '1506582917', '1', null, null, '', null, null, '[\"Settings\"]', '', 'menu');
INSERT INTO `menu` VALUES ('16', '1', '2', 'ClearCache', '/site/clear-cache', '', 'circle-o-notch', '1', null, '', '', '1', '10', '*', '', null, '1506584324', '1', null, null, '', null, null, '[\"Settings\"]', '', '');
