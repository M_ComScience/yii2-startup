<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use kartik\icons\Icon;
use msoft\core\classes\JSQuery;
use msoft\helpers\RegisterJS;
use msoft\core\classes\CoreFunc;
use msoft\core\classes\CoreQuery;
use yii\helpers\ArrayHelper;

#Register JS
RegisterJS::regis(['sweetalert','codemirror'],$this);
JSQuery::generateJS('core/core-scripts/update',$this);

$modelFields = CoreQuery::getAllOptionsTable('core_scripts','/core/core-scripts/update');
?>

<style>
.CodeMirror {
  border: 1px solid #eee;
  height: auto;
  font-size:11pt;
}
</style>
<div class="tb-js-scripts-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL,'id' => 'form-core-scripts']);?>

    <?php
        if (!empty($modelFields)) {
            foreach ($modelFields as $key => $value) {
                echo CoreFunc::generateInput($value, $model, $form, "table_varname");
            }
        }
    ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group" style="text-align:right;">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Close',Yii::$app->request->referrer, ['class' => 'btn btn-default'])?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>