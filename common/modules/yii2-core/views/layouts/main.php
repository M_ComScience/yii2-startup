<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use msoft\core\assets\CoreAsset;

AppAsset::register($this);
CoreAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Core Modules',
        'brandUrl' => '/core/core-fields/index',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        //['label' => 'Core Dynamic Form', 'url' => ['/core/tables-fields/list-data']],
        ['label' => 'Core Widgets', 'url' => ['/core/core-fields/index']],
        ['label' => 'Core Forms', 'url' => ['/core/core-forms/index']],
        ['label' => 'Core Items', 'url' => ['/core/core-item-alias/index']],
        //['label' => 'Tables Fields', 'url' => ['/core/tables-fields/index']],
        ['label' => 'Core', 'url' => ['#'],
            'items' => [
                ['label' => 'Tables Rules', 'url' =>['/core/tables-fields/rules']],
                ['label' => 'Core Migration', 'url' =>['/core/core-migration/index']],
                ['label' => 'Core Editor', 'url' =>['/core/core-editor/index']],
                ['label' => 'Core Scripts', 'url' =>['/core/core-scripts/index']],
            ]
        ],
        
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Yii2 Core <?= date('Y') ?></p>

        <p class="pull-right"><?= 'Powered by MSoft' ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
