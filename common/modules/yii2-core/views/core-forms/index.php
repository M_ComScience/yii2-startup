<?php

use msoft\helpers\Html;
use msoft\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use msoft\core\widgets\ModalForm;
use msoft\helpers\Noty;
use msoft\widgets\SwalAlert;
use msoft\widgets\Panel;
use yii\bootstrap\Modal;
use msoft\core\models\CoreForms;

$this->title = 'Core Forms';
$this->params['breadcrumbs'][] = $this->title;

echo SwalAlert::widget();
?>
<style>
.modal {
	z-index:16000 !important;
}
</style>
<div class="core-forms-index">
<h3><?= Html::encode($this->title) ?><small> @MSoft</small> 
<p style="text-align:right;">
	<small><?= Html::a('Read Document','http://demos.krajee.com/builder-details/form',['target' => '_blank']) ?></small></h3>
</p>
<?php 
echo Panel::begin([
    'title'=>'Example Code',
    'widget'=>true, // no js included (ie. style only)
	'footer'=>false,
	'collapse'=>true,
]);
?>
<pre>
use msoft\helpers\Html; 
use msoft\core\classes\CoreQuery;
use msoft\core\classes\CoreFunc;
use msoft\widgets\ActiveForm;
use msoft\core\models\TablesFields;

$attributes = CoreQuery::getAttributesForm('TablesFields');
$model = new TablesFields();
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);

if(isset($attributes)){
    echo CoreFunc::renderInput($model,$form,$attributes);
}
echo Html::submitButton('Submit', ['class'=>'btn btn-primary']);
ActiveForm::end();
</pre>
<?php
Panel::end();
?>
<div class="core-forms-index">
<?php Pjax::begin(['id'=>'core-forms-grid-pjax']); ?>    
<?= GridView::widget([
        'id' => 'core-forms-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
        	'heading'=> false,
			'before'=>	Html::a(Html::getBtnAdd(),['core-forms/create-dynamic-form'],['class' => 'btn btn-success btn-sm','data-pjax' => 0]).'  '.
					  	Html::button(Html::getBtnDelete(), ['data-url'=>Url::to(['core-forms/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-core-forms', 'disabled'=>true]),
			'after'=>false,
        ],
        'hover' => true,
		'pjax' => true,
		'condensed' => true,
		'export' => [
			'fontAwesome' => true
		],
        'columns' => [
            [
				'class' => 'msoft\widgets\grid\CheckboxColumn',
				'checkboxOptions' => [
					'class' => 'selectionCoreFormsIds'
				],
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:40px;text-align: center;'],
			],
			[
				'attribute'=>'action_id',
                'value'=>function ($model, $key, $index, $widget) { 
                    return '';
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=> ArrayHelper::map(CoreForms::find()->all(), 'action_id', 'action_id'), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
				'filterInputOptions'=>['placeholder'=>'Action'],
			],
			[
				'attribute'=>'action_id',
                'value'=>function ($model, $key, $index, $widget) { 
					return $model['action_id'].' '.Html::a('RawSql',false,['id' => 'btn-get-rawsql','class' => 'btn btn-sm btn-default','data-url' => Url::to(['get-rawsql','actionid' => $model['action_id']])]).' '.
					Html::a('Edit',['update-dynamic-form','id' => $model['action_id']],['class' => 'btn btn-sm btn-primary','data-pjax' => 0]);
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=> ArrayHelper::map(CoreForms::find()->all(), 'action_id', 'action_id'),
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
				'filterInputOptions'=>['placeholder'=>'Action'],
				'group'=>true,  // enable grouping,
                'groupedRow'=>true,                    // move grouped column to a single grouped row
                'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
                'groupEvenCssClass'=>'kv-grouped-row', // configure even group cell css class
				'format' => 'raw'
			],
			[
				'attribute'=>'status',
				'class'=>'\msoft\widgets\ToggleColumn',
				'linkTemplateOn'=>'<a class="toggle-column btn btn-success btn-xs btn-block" data-pjax="0" href="{url}"><i  class="glyphicon glyphicon-ok"></i> {label}</a>',
				'linkTemplateOff'=>'<a class="toggle-column btn btn-danger btn-xs btn-block" data-pjax="0" href="{url}"><i  class="glyphicon glyphicon-remove"></i> {label}</a>'
			],
			[
				'class'=>'msoft\widgets\grid\EditableColumn',
				'attribute'=>'row', 
				'editableOptions'=>[
					'header'=>'Row', 
					'inputType'=>\msoft\widgets\Editable::INPUT_TEXT,
					'options'=>[
						'pluginOptions'=>[]
					],
					'formOptions' => ['action' => ['/core/core-forms/save-editable','attr' => 'row']],
				],
				'vAlign'=>'middle',
				'hAlign' => 'center',
			],
			[
				'attribute'=>'table_name', 
				'vAlign'=>'middle',
				'hAlign' => 'center',
				'group'=>true,
			],
            'attributes',
            'label',
            'type',
            [
				'class'=>'msoft\widgets\grid\EditableColumn',
				'attribute'=>'order', 
				'editableOptions'=>[
					'header'=>'Oder', 
					'inputType'=>\msoft\widgets\Editable::INPUT_TEXT,
					'options'=>[
						'pluginOptions'=>[]
					],
					'formOptions' => ['action' => ['/core/core-forms/save-editable','attr' => 'order']],
				],
				'vAlign'=>'middle',
				'hAlign' => 'center'
			],
			'widgetClass',
            [
                'class' => 'msoft\widgets\ActionColumn',
                'template'=>'<div class="btn-group btn-group-sm" style="width:100px;"> {view} {update} {delete} </div>',
				'template' => '{view} {delete}',
				'noWrap' => true,
				'viewOptions' => [
					'class' => 'btn btn-sm btn-default action',
					'data-action' => 'view'
                ],
                'deleteOptions' => [
					'class' => 'btn btn-sm btn-default action',
					'data-action' => 'delete'
				],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>

<?=  ModalForm::widget([
    'id' => 'modal-core-forms',
    'size' => 'modal-lg modal-full',
	'options' => ['tabindex' => false,],
]);
?>
<?php
Modal::begin([
	"id" => "ajaxCrudModal",
	'header' => '<h4 class="modal-title">RawSql</h4>',
    "footer" => Html::button('Close',['class' => 'btn btn-default','data-dismiss' => 'modal']), // always need it for jquery plugin
    "size" => "modal-lg"
])
?>
<pre id="raw-sql">
	
</pre>
<?php Modal::end(); ?>
<?php  $this->registerJs("
$('#core-forms-grid-pjax').on('click', '#modal-addbtn-core-forms', function(){
    modalCoreForms($(this).attr('data-url'));
});

$('#core-forms-grid-pjax').on('click', '#modal-delbtn-core-forms', function(){
    selectionCoreFormsGrid($(this).attr('data-url'));
});

$('#core-forms-grid-pjax').on('click', '.select-on-check-all', function(){
    window.setTimeout(function() {
		var key = $('#core-forms-grid').yiiGridView('getSelectedRows');
		disabledCoreFormsBtn(key.length);
    },100);
});

$('#core-forms-grid-pjax').on('click', '.selectionCoreFormsIds', function(){
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledCoreFormsBtn(key.length);
});

$('#core-forms-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalCoreForms('".Url::to(['core-forms/update', 'id'=>''])."'+id);
});
$('#core-forms-grid-pjax').on('click', 'tbody tr td a.action', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'view'){
		modalCoreForms(url);
	}else if(action === 'update'){ 
		window.location.href = url;
	}else if(action === 'delete') {
		yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
			$.post(
				url
			).done(function(result){
				if(result.status == 'success'){
					". Noty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#core-forms-grid-pjax'});
				} else {
					". Noty::show('result.message', 'result.status') ."
				}
			}).fail(function(){
				". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
				console.log('server error');
			});
		})
    }
    return false;
});

$('#core-forms-grid-pjax').on('click','#btn-get-rawsql', function() {
	var url = $(this).attr('data-url');
	$.get(
		url
	).done(function(result){
		$('#ajaxCrudModal').modal('show');
		$('#raw-sql').text(result);
	}).fail(function(){
		console.log('server error');
	});
});

function disabledCoreFormsBtn(num){
    if(num>0){
		$('#modal-delbtn-core-forms').attr('disabled', false);
    } else{
		$('#modal-delbtn-core-forms').attr('disabled', true);
    }
}

function selectionCoreFormsGrid(url){
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function(){
		$.ajax({
			method: 'POST',
			url: url,
			data: $('.selectionCoreFormsIds:checked[name=\"selection[]\"]').serialize(),
			dataType: 'JSON',
			success: function(result, textStatus) {
				if(result.status == 'success') {
					". Noty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#core-forms-grid-pjax'});
				} else {
					". Noty::show('result.message', 'result.status') ."
				}
			}
		})
	
    })
}

function modalCoreForms(url) {
    $('#modal-core-forms .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-core-forms').modal('show')
    .find('.modal-content')
    .load(url);
}
function init_click_handlers(){
	$(\"a.toggle-column\").on(\"click\", function(e) {
		e.preventDefault();
		var pjaxId = $(e.target).closest(\".grid-view\").parent().attr(\"id\");
		$(this).button('loading');
		$.post($(this).attr(\"href\"), function(data) {
		  $.pjax.reload({container:\"#\" + pjaxId});
		});
		return false;
	});
}
init_click_handlers(); //first run
$(\"#core-forms-grid-pjax\").on(\"pjax:success\", function() {
  init_click_handlers(); //reactivate links in grid after pjax update
});
");?>

<?php
/*
$attributes = CoreQuery::getAttributesForm('TablesFields');
$model  = new TablesFields();
$form = ActiveForm::begin([]);

if(isset($attributes)){
    echo CoreFunc::renderInput($model,$form,$attributes);
}
echo Html::submitButton('Submit', ['class'=>'btn btn-primary']);
ActiveForm::end();
*/
?>