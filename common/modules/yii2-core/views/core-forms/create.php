<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model msoft\core\models\CoreForms */

$this->title = 'Create Core Forms';
$this->params['breadcrumbs'][] = ['label' => 'Core Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-forms-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
