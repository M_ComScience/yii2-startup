<?php 
use msoft\widgets\ActiveForm;
use msoft\helpers\Html;
use msoft\helpers\ArrayHelper;
use msoft\widgets\DynamicFormWidget;
use msoft\widgets\Icon;
use msoft\widgets\Typeahead;
use yii\helpers\Json;
use msoft\core\classes\CoreFunc;
use msoft\widgets\Select2;
use msoft\widgets\CheckboxX;
use msoft\core\assets\codemirror\CodeMirrorAsset;
use msoft\widgets\TouchSpin;
use yii\bootstrap\Modal;
use msoft\helpers\RegisterJS;
use msoft\core\widgets\ModalForm;
use msoft\widgets\DepDrop;
use yii\helpers\Url;
use msoft\core\models\CoreForms;
use msoft\widgets\Form;
use msoft\core\models\CoreFields;
use \msoft\widgets\AlertBlock;
use msoft\widgets\growl\GrowlAsset;

GrowlAsset::register($this);
CodeMirrorAsset::register($this);
RegisterJS::regis(['sweetalert','ajaxcrud'],$this);
$this->title = 'Dynamic Form';

$schema = Yii::$app->db->schema;
$Tablenames = [];
foreach($schema->getTablenames() as $name){
    $Tablenames[$name] = $name;
}

$query = CoreForms::find()->all();

$this->registerCss("
.line-dashed {
    background-color: transparent;
    border-bottom: 1px dashed #c9302c !important;
}
.CodeMirror {
    border: 1px solid #eee;
    height: auto;
}
");

echo AlertBlock::widget([
    'useSessionFlash' => true,
    'type' => AlertBlock::TYPE_ALERT
]);
?>
<?php $form = ActiveForm::begin(['id' => 'dynamic-form']);?>
    <div class="dynamic-form">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 50, // the maximum times, an element can be cloned (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $models[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'action_id',
            ],
        ]); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::button(Icon::show('plus').' เพิ่ม',['class' => 'add-item btn btn-success btn-sm']); ?>
                <?= Html::button(' Show all',['class' => 'show-all btn btn-default btn-sm']); ?>
                <?= Html::button(' Hide all',['class' => 'hide-all btn btn-default btn-sm']); ?>
                <span class="pull-right"><?= Html::encode('Core Forms'); ?></span>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->
                <?php foreach ($models as $index => $model): ?>
                    <div class="item"><!-- widgetBody -->
                        <div class="padding-v-sm">
                            <div class="line line-dashed"></div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="pull-left title-dynamicform" style="font-size:12pt;"><?= Html::encode('Fields# '); ?><?= $model['label']; ?> <?= ($index + 1) ?> </span>
                            </div>
                            <div class="col-sm-6" style="text-align:right;">
                                <?= Html::a('Show',false,['class' => 'show-content-form btn btn-primary btn-xs','onclick' => 'Dynamic.toggleContent(this);']); ?>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i> ลบ</button>
                            </div>
                        </div>
                        <br>
                        <?php
                            // necessary for update action.
                            if (!$model->isNewRecord) {
                                echo Html::activeHiddenInput($model, "[{$index}]form_id");
                            }
                        ?>

                        <div class="content-form">
                            <div class="row">
                                <div class="col-md-3">
                                    <?php 
                                        echo $form->field($model, "[{$index}]row")->widget(TouchSpin::classname(), [
                                            'options'=>['placeholder'=>'Row...'],
                                            'pluginOptions' => [
                                                'buttonup_class' => 'btn btn-default', 
                                                'buttondown_class' => 'btn btn-default', 
                                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                                            ]
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-3">
                                    <?=
                                        $form->field($model, "[{$index}]action_id")->widget(Typeahead::classname(), [
                                            'options' => ['placeholder' => 'Filter Action...'],
                                            'pluginOptions' => ['highlight' => true, 'minLength' => 0],
                                            'dataset' => [
                                                [
                                                    'local' => count($query) > 0 ? ArrayHelper::getColumn($query,'action_id') : [''],
                                                    'limit' => 50
                                                ]
                                            ]
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-3">
                                    <?=
                                        $form->field($model, "[{$index}]table_name")->widget(Select2::classname(), [
                                            'data' => $Tablenames,
                                            'options' => ['placeholder' => 'Select field ...'],
                                            'pluginOptions' => [
                                                    'allowClear' => true
                                            ],
                                            'pluginEvents' => [
                                                //"change" => "function() { Dynamic.initTypeaHeadOnchange($(this).val(),".$index."); }",
                                            ]
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-3">
                                    <?= $form->field($model, "[{$index}]attributes")->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, "[{$index}]label")->textInput([]) ?>
                                </div>

                                <div class="col-md-4">
                                    <?=
                                        $form->field($model, "[{$index}]type")->widget(Select2::classname(), [
                                            'data' => [
                                                Form::INPUT_TEXT => 'INPUT_TEXT',
                                                Form::INPUT_TEXTAREA => 'INPUT_TEXTAREA',
                                                Form::INPUT_PASSWORD => 'INPUT_PASSWORD',
                                                Form::INPUT_DROPDOWN_LIST => 'INPUT_DROPDOWN_LIST',
                                                Form::INPUT_LIST_BOX => 'INPUT_LIST_BOX',
                                                Form::INPUT_CHECKBOX => 'INPUT_CHECKBOX',
                                                Form::INPUT_RADIO => 'INPUT_RADIO',
                                                Form::INPUT_CHECKBOX_LIST => 'INPUT_CHECKBOX_LIST',
                                                Form::INPUT_RADIO_LIST => 'INPUT_RADIO_LIST',
                                                Form::INPUT_MULTISELECT => 'INPUT_MULTISELECT',
                                                Form::INPUT_FILE => 'INPUT_FILE',
                                                Form::INPUT_HTML5 => 'INPUT_HTML5',
                                                Form::INPUT_WIDGET => 'INPUT_WIDGET',
                                                Form::INPUT_STATIC => 'INPUT_STATIC',
                                                Form::INPUT_HIDDEN => 'INPUT_HIDDEN',
                                                Form::INPUT_HIDDEN_STATIC => 'INPUT_HIDDEN_STATIC',
                                                Form::INPUT_RAW => 'INPUT_RAW',
                                                Form::INPUT_CHECKBOX_BUTTON_GROUP => 'INPUT_CHECKBOX_BUTTON_GROUP',
                                                Form::INPUT_RADIO_BUTTON_GROUP => 'INPUT_RADIO_BUTTON_GROUP'
                                            ],
                                            'options' => ['placeholder' => 'Select Input type ...'],
                                            'pluginOptions' => [
                                                    'allowClear' => true
                                            ],
                                        ])->label('Input Type');
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?= $form->field($model, "[{$index}]visible")->textInput([]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, "[{$index}]labelSpan")->textInput([]) ?>
                                </div>

                                <div class="col-md-4">
                                    <?=
                                        $form->field($model, "[{$index}]widgetClass")->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(CoreFields::find()->where(['<>', 'field_class', ''])->asArray()->all(),'field_class','field_class'),
                                            'options' => ['placeholder' => 'Select WidgetClass ...'],
                                            'pluginOptions' => [
                                                    'allowClear' => true
                                            ],
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php 
                                        echo $form->field($model, "[{$index}]order")->widget(TouchSpin::classname(), [
                                            'options'=>['placeholder'=>'Order...'],
                                            'pluginOptions' => [
                                                'buttonup_class' => 'btn btn-default', 
                                                'buttondown_class' => 'btn btn-default', 
                                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                                            ]
                                        ]);
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, "[{$index}]columns")->textInput([]) ?>
                                </div>
                                <div class="col-md-8">
                                    <?= $form->field($model, "[{$index}]hint")->textInput([]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]value")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]options")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]fieldConfig")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]columnOptions")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]labelOptions")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]items")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
    <div class="modal-footer">
        <?= Html::a('Close',['/core/core-forms/index'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$actionid = Json::encode(ArrayHelper::getColumn($query,'action_id'));
$this->registerJs(<<<JS
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    var indexs = [];

    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $(this).attr("content-form","content-form-"+ (index + 1));
    });

    jQuery(".dynamicform_wrapper div.content-form").each(function(index) {
        $(this).addClass("content-form-"+ (index + 1));
    });

    jQuery(".dynamicform_wrapper .title-dynamicform").each(function(index) {
        jQuery(this).html("Fields "  + (index + 1) +' '+ $('#coreforms-'+index+'-label').val() + ' #Row' + $('#coreforms-'+index+'-row').val());
        indexs.push(parseInt(index));
    });
    Dynamic.initCodeMirror(indexs);
    Dynamic.hideContentForm(indexs);
    Dynamic.initTypeaHead(indexs);
    Dynamic.initSelect2(indexs);
    Dynamic.defaultValue(indexs);
    $('html, body').animate({
        scrollTop: $('footer').offset().top
    }, 'slow');
    //
});

Dynamic = {
    initCodeMirror: function(indexs){
        var myTextAreainput_value = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-value');
        CodeMirror.fromTextArea(myTextAreainput_value,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_options = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-options');
        CodeMirror.fromTextArea(myTextAreainput_options,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_fieldconfig = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-fieldconfig');
        var fieldconfig = CodeMirror.fromTextArea(myTextAreainput_fieldconfig,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});
        //fieldconfig.getDoc().setValue("['showLabels' => false1]");

        var myTextAreainput_columnoptions = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-columnoptions');
        var columnoptions = CodeMirror.fromTextArea(myTextAreainput_columnoptions,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});
        //columnoptions.getDoc().setValue("['colspan' => 12]");

        var myTextAreainput_labeloptions = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-labeloptions');
        var labeloptions = CodeMirror.fromTextArea(myTextAreainput_labeloptions,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});
        //labeloptions.getDoc().setValue("['class' => 'col-sm-2 control-label']");

        var myTextAreainput_items = document.getElementById('coreforms-'+Math.max.apply(null, indexs)+'-items');
        CodeMirror.fromTextArea(myTextAreainput_items,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});
    },
    hideContentForm : function(indexs){
        //init form content hide
        var maxindex = Math.max.apply(null, indexs) + 1;
        $('div.content-form-' + maxindex ).css('display','none');
    },
    toggleContent : function(e){
        if($('div.' + $(e).attr('content-form')).css('display') == 'none'){
            e.innerHTML = 'Hide';
            $('div.' + $(e).attr('content-form')).css('display','block');
        }else{
            e.innerHTML = 'Show';
            $('div.' + $(e).attr('content-form')).css('display','none');
        }
    },
    initSelect2: function(indexs){
        var index = Math.max.apply(null, indexs);
        if (jQuery('#coreforms-'+index+'-table_name').data('select2')) { 
            jQuery('#coreforms-'+index+'-table_name').select2('destroy'); 
        }
        jQuery.when(
            jQuery('#coreforms-'+index+'-table_name').select2({
                "allowClear":true,
                "theme":"krajee",
                "width":"100%",
                "placeholder":
                "Select field ...",
                "language":"th-TH"
        })).done(
            initS2Loading('coreforms-'+index+'-table_name',{"themeCss":".select2-container--krajee","sizeCss":"","doReset":true,"doToggle":false,"doOrder":false}));
        jQuery('#coreforms-'+index+'-table_name').on('change', function() {
            Dynamic.initTypeaHeadOnchange($(this).val(),index); 
        });
    },
    initTypeaHeadOnchange : function(tb_name,index){
        var columns;
        $.ajax({
            type: 'POST',
            url: 'get-column-names',
            data: {tb_name:tb_name},
            async: false,
            dataType: "json",
            success: function (result) {
                if(result){
                    columns = $.map(result, function(el) { return el });
                }
            },
            error: function (xhr, status, error) {
                swal(error, status, "error");
            },
        });
        
        var tablesfields = new Bloodhound({
            "datumTokenizer":Bloodhound.tokenizers.whitespace,
            "queryTokenizer":Bloodhound.tokenizers.whitespace,
            "local":columns
        });

        kvInitTA('coreforms-'+index+'-attributes', {"highlight":true,"minLength":0}, [{
            "limit":50,
            "name":"coreforms",
            "source":tablesfields.ttAdapter()
        }]);
    },
    initTypeaHead : function (indexs){
        var index = Math.max.apply(null, indexs);
        var coreforms_action_id_data = new Bloodhound({
            "datumTokenizer":Bloodhound.tokenizers.whitespace,
            "queryTokenizer":Bloodhound.tokenizers.whitespace,
            "local":{$actionid}
        });
        kvInitTA('coreforms-'+index+'-action_id', {"highlight":true,"minLength":0}, [{
            "limit":50,
            "name":"coreforms_action_id_data",
            "source":coreforms_action_id_data.ttAdapter()
        }]);
    },
    initForm : function (){
        var indexs = [];
        jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
            $(this).attr("content-form","content-form-"+ (index + 1));
        });

        jQuery(".dynamicform_wrapper div.content-form").each(function(index) {
            $(this).addClass("content-form-"+ (index + 1));
        });

        jQuery(".dynamicform_wrapper .title-dynamicform").each(function(index) {
            jQuery(this).html("Fields "  + (index + 1) +' '+ $('#coreforms-'+index+'-label').val() + ' #Row' + $('#coreforms-'+index+'-row').val());
            indexs.push(parseInt(index));
            Dynamic.initCodeMirror([index]);
            Dynamic.hideContentForm([index]);
            Dynamic.initTypeaHead([index]);
        });
    },
    defaultValue:function(indexs){
        var newid = Math.max.apply(null, indexs) + 1;
        var lastid = Math.max.apply(null, indexs); //ที่กดเพิ่มล่าสุด
        $('#coreforms-' + lastid + '-row').val(newid);
        $('#coreforms-' + lastid + '-order').val(newid);
        $('div.content-form-'+newid).css('display','block');
        $('#coreforms-'+lastid+'-action_id').val($('#coreforms-'+(lastid-1)+'-action_id').val());
        $('#coreforms-'+lastid+'-table_name').val($('#coreforms-'+(lastid-1)+'-table_name').val()).trigger("change");
        $('#coreforms-'+lastid+'-type').val('textInput').trigger("change");
    }
};
$('button.show-all').on('click',function(){
    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $('div.content-form-'+(index+1)).css('display','block');
        var e = $('a.show-content-form');
        e[index].innerHTML = 'Hide';
    });
});
$('button.hide-all').on('click',function(){
    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $('div.content-form-'+(index+1)).css('display','none');
        var e = $('a.show-content-form');
        e[index].innerHTML = 'Show';
    });
});
Dynamic.initForm();
JS
);?>
<?php
if(Yii::$app->controller->action->id == 'update-dynamic-form'){
$this->registerJs('
$("form#dynamic-form").on("beforeSubmit", function(e){
    var form = $(this);
    var data = new FormData($(form)[0]);
    $.ajax({
        "type":form.attr("method"),
        "url": form.attr("action"),
        "data":data,
        "async": false,
        "dataType":"json",
        "success":function (result) {
            AppNotify.SaveCompleted();
        },
        "error":function (xhr, status, error) {
            AppNotify.Error(error);
        },
        contentType: false,
        cache: false,
        processData: false
    });
    return false;
});
');
}
?>
<?=  Modal::widget([
    'header' => '<h4 class="modal-title">Code Preview</h4>',
    'id' => 'modal-tables-fields',
    'size' => 'modal-lg',
    'options' => ['tabindex' => false,],
    'footer' => Html::button('Close',['class' => 'btn btn-default' , 'data-dismiss' => 'modal'])
]);
?>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => "modal-lg"
])
?>
<?php Modal::end(); ?>