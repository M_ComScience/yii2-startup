<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model metronic\core\models\CoreGenerate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-generate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gen_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gen_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gen_tag')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gen_link')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gen_process')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gen_ui')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'template_php')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'template_html')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'template_js')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
