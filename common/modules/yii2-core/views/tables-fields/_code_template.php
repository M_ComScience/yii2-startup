<?php
use yii\helpers\Json;
use msoft\utils\CoreUtility;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use msoft\core\classes\CoreFunc;
?>

<?php
$options = ArrayHelper::merge(CoreUtility::string2Array($model['field_meta']), CoreUtility::string2Array($model['input_meta']));
$options = CoreUtility::array2String($options);
$options = CoreUtility::string2strArray($options);
$specific = CoreUtility::string2strArray($model['input_specific']);
$label = $model['input_label'];
if ($model['field_name'] !== NULL) {
    if ($model['field_internal'] == 1) {
        echo Html::encode($model['begin_html']).'<br>';
        if ($model['field_name'] == 'widget') {
            if($model['field_code'] == 'Select2'){
                $input_data = $model['input_data'];
				eval("\$data = {$input_data};");
                $options = ArrayHelper::merge(CoreUtility::string2Array($model['input_meta']), ['data' =>$data]);
                $options = CoreUtility::array2String($options);
                $options = CoreUtility::string2strArray($options);
            }
            if(!empty($label)){
                echo '<span class="text-danger">'.Html::encode('<?= ').("\$form->field(\$model, '{$model['table_varname']}',{$specific})->hint({$model['input_hint']})->widget({$model['field_class']}, {$options})->label('{$label}');?>".'</span>'."\n");
            }else{
                echo '<span class="text-danger">'.Html::encode('<?= ').("\$form->field(\$model, '{$model['table_varname']}',{$specific})->hint({$model['input_hint']})->widget({$model['field_class']}, {$options})->label(false);?>".'</span>'."\n");
            }
                
        } else {
            if (empty($model['input_data'])) {
                echo '<span class="text-danger">'.Html::encode('<?= ').("\$form->field(\$model, '{$model['table_varname']}', {$specific})->hint({$model['input_hint']})->{$model['field_name']}({$options});?>".'</span>'."\n");
            } else {
                echo Html::encode('<?= ').("\$form->field(\$model, '{$model['table_varname']}', {$specific})->hint({$model['input_hint']})->{$model['field_name']}({$model['input_data']}, {$options});?>"."\n");
            }
        }
        echo Html::encode($model['end_html']);
    }else{
        $options = CoreUtility::string2Array($model['input_meta']);
        $options = CoreUtility::array2String($options);
        $options = CoreUtility::string2strArray($options);
        $html = '';
        if ($model['field_name'] == 'widget') {
            $options['model'] = $model;
            $options['attribute'] = $model['table_varname'];
            echo ("{$model['field_class']}::{$model['field_name']}({$options});");
        } else {
            if (empty($model['input_data'])) {
            echo ("{$model['field_class']}::{$model['field_name']}(\$model, '{$model['table_varname']}', {$options});");
            } else {
            echo ("{$model['field_class']}::{$model['field_name']}(\$model, {$model['table_varname']}, {$option['input_data']}, {$options});");
            }
        }
    }
}
?>