<?php

use msoft\widgets\GridView;
use msoft\core\widgets\ModalForm;
use msoft\helpers\Noty;
use msoft\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use msoft\core\assets\CoreAsset;

CoreAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\CoreOptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Options';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.modal-dialog {
    width: 99%;
}
</style>
<div class="core-options-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
 <?php  Pjax::begin(['id'=>'core-options-grid-pjax']);?>
    <?= GridView::widget([
		'id' => 'core-options-grid',
		'panel' => [
        	'heading'=> false,
			'before'=>	Html::button(Html::getBtnAdd(), ['data-url'=>Url::to(['core-options/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-core-options']). ' ' .
					  Html::button(Html::getBtnDelete(), ['data-url'=>Url::to(['core-options/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-core-options', 'disabled'=>true]),
			'after'=>false,
		],
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'hover' => true,
		'pjax' => true,
		'condensed' => true,
		'export' => [
			'fontAwesome' => true
		],
        'columns' => [
            [
				'class' => 'yii\grid\CheckboxColumn',
				'checkboxOptions' => [
					'class' => 'selectionCoreOptions'
				],
				'headerOptions' => ['style'=>'text-align: center;'],
				'contentOptions' => ['style'=>'width:40px;text-align: center;'],
			],
            [
				'class' => 'yii\grid\SerialColumn',
				'headerOptions' => ['style'=>'text-align: center;'],
				'contentOptions' => ['style'=>'width:60px;text-align: center;'],
			],
            'option_name',
            'option_value:ntext',
            'autoload',
            'input_label',
            // 'input_hint:ntext',
            // 'input_field',
            // 'input_specific:ntext',
            // 'input_data:ntext',
            // 'input_required',
            // 'input_validate:ntext',
            // 'input_meta:ntext',
            // 'input_order',

            [
				'class' => 'msoft\widgets\ActionColumn',
				'contentOptions'=>['style'=>'width:80px;text-align: center;'],
			],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
<?=  ModalForm::widget([
    'id' => 'modal-core-options',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#core-options-grid-pjax').on('click', '#modal-addbtn-core-options', function(){
    modalCoreField($(this).attr('data-url'));
});

$('#core-options-grid-pjax').on('click', '#modal-delbtn-core-options', function(){
    selectionCoreFieldGrid($(this).attr('data-url'));
});

$('#core-options-grid-pjax').on('click', '.select-on-check-all', function(){
    window.setTimeout(function() {
		var key = $('#core-options-grid').yiiGridView('getSelectedRows');
		disabledCoreFieldBtn(key.length);
    },100);
});

$('#core-options-grid-pjax').on('click', '.selectionCoreOptions', function(){
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledCoreFieldBtn(key.length);
});

$('#core-options-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalCoreField('".Url::to(['core-options/update', 'id'=>''])."'+id);
});	

$('#core-options-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
		modalCoreField(url);
    } else if(action === 'delete') {
		yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
			$.post(
				url
			).done(function(result){
			if(result.status == 'success'){
				". Noty::show('result.message', 'result.status') ."
				$.pjax.reload({container:'#core-options-grid-pjax'});
			} else {
				". Noty::show('result.message', 'result.status') ."
			}
			}).fail(function(){
				". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
				console.log('server error');
			});
		})
    }
    return false;
});

function disabledCoreFieldBtn(num){
    if(num>0){
		$('#modal-delbtn-core-options').attr('disabled', false);
    } else{
		$('#modal-delbtn-core-options').attr('disabled', true);
    }
}

function selectionCoreFieldGrid(url){
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function(){
		$.ajax({
			type: 'POST',
			url: url,
			data: $('.selectionCoreOptions:checked[name=\"selection[]\"]').serialize(),
			dataType: 'JSON',
			success: function(result) {
				if(result.status == 'success') {
					". Noty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#core-options-grid-pjax'});
				} else {
					". Noty::show('result.message', 'result.status') ."
				}
			}
		})
	
    })
}

function modalCoreField(url) {
    $('#modal-core-options .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-core-options').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>