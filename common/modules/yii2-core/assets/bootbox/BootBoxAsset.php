<?php

namespace msoft\core\assets\bootbox;

use yii\web\AssetBundle;

class BootBoxAsset extends AssetBundle {

	public $sourcePath = '@msoft/core/assets/bootbox';
	public $css = [
	];
	public $js = [
		'js/bootbox.min.js',
		'js/confirm.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];

}
