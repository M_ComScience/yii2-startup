<?php

use yii\db\Schema;

class m170928_030101_tables_fields extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('tables_fields', [
            'table_id' => $this->primaryKey(),
            'table_name' => $this->string(50)->notNull(),
            'table_varname' => $this->string(50)->notNull(),
            'table_field_type' => $this->string(50)->notNull(),
            'table_length' => $this->string(10)->notNull(),
            'table_default' => $this->text()->notNull(),
            'table_index' => $this->string(50)->notNull(),
            'input_field' => $this->string(20)->notNull(),
            'input_label' => $this->string(100)->notNull(),
            'input_hint' => $this->text()->notNull(),
            'input_specific' => $this->text()->notNull(),
            'input_data' => $this->text()->notNull(),
            'input_required' => $this->smallInteger(1)->notNull(),
            'input_validate' => $this->text()->notNull(),
            'input_meta' => $this->text()->notNull(),
            'input_order' => $this->smallInteger(3)->notNull(),
            'action_id' => $this->string(255),
            'begin_html' => $this->text(),
            'end_html' => $this->text(),
            'updated_at' => $this->datetime()->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'status' => $this->integer(11),
            ], $tableOptions);

        $path = __DIR__.'/sql';
        $filenames = $this->dirToArray($path);
        if(is_array($filenames)){
            foreach($filenames as $file){
                if(file_exists($path.'/'.$file)){
                    $this->execute(file_get_contents($path.'/'.$file));
                }
            }
        } 
                
    }

    public function down()
    {
        $this->dropTable('tables_fields');
    }

    function dirToArray($dir) { 
        
        $result = array(); 
     
        $cdir = scandir($dir); 
        foreach ($cdir as $key => $value) 
        { 
           if (!in_array($value,array(".",".."))) 
           { 
              if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
              { 
                 $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
              } 
              else 
              { 
                 $result[] = $value; 
              } 
           } 
        } 
        
        return $result; 
    }
}
