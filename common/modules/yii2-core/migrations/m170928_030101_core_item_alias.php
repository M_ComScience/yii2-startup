<?php

use yii\db\Schema;

class m170928_030101_core_item_alias extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('core_item_alias', [
            'item_code' => $this->string(50)->notNull(),
            'item_name' => $this->string(200)->notNull(),
            'item_data' => $this->text()->notNull(),
            'PRIMARY KEY ([[item_code]])',
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('core_item_alias');
    }
}
