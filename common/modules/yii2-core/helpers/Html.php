<?php

namespace msoft\helpers;

use Yii;
use yii\helpers\BaseHtml;
use msoft\widgets\Icon;

class Html extends BaseHtml {

	public static function getMsgSuccess() {
		return '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ';
	}

	public static function getMsgError() {
		return '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ';
	}

	public static function getBtnAdd() {
		return '<span class="glyphicon glyphicon-plus"></span>';
	}

	public static function getBtnDelete() {
		return '<span class="glyphicon glyphicon-minus"></span>';
	}

	public static function getBtnRepeat() {
		return '<span class="glyphicon glyphicon-repeat"></span>';
	}

	public static function textDelete(){
		return Icon::show('trash-o').Yii::t('yii','Delete');
	}

	public static function textEdit(){
		return Icon::show('icon-pencil',[],Icon::I).'แก้ไข';
	}

	public static function textView(){
		return Icon::show('icon-eye',[],Icon::I).Yii::t('yii','View');
	}
	
}