<?php

namespace msoft\helpers\interfaces;

/**
 * Interface Arrayable
 *
 * @package msoft\helpers\interfaces
 */
interface Arrayable
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray();
}
