<?php

namespace msoft\helpers\interfaces;

/**
 * Interface Jsonable
 *
 * @package msoft\helpers\interfaces
 */
interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     *
     * @return string
     */
    public function toJson($options = 0);
}
