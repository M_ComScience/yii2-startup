<?php

namespace msoft\widgets\color;

class ColorInputAsset extends \msoft\widgets\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/spectrum', 'css/spectrum-kv']);
        $this->setupAssets('js', ['js/spectrum', 'js/spectrum-kv']);
        parent::init();
    }
}
