<?php

namespace msoft\widgets\grid;

use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for [[RadioColumn]] functionality of the [[GridView]] widget.
 *
 */
class RadioColumnAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-grid-radio']);
        parent::init();
    }
}
