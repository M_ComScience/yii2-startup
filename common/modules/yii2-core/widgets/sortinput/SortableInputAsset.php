<?php

namespace msoft\widgets\sortinput;

/**
 * SortableInput bundle for \msoft\widgets\sortinput\SortableInput
 *
 */
class SortableInputAsset extends \msoft\widgets\base\AssetBundle
{

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-sortable-input']);
        parent::init();
    }

}