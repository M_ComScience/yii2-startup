<?php

namespace msoft\widgets\fileupload;

use yii\web\AssetBundle;

class FileUploadUIAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/blueimp-file-upload';
    public $css = [
        'css/jquery.fileupload.css'
    ];
    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload.js',
        'js/jquery.fileupload-process.js',
        'js/jquery.fileupload-image.js',
        'js/jquery.fileupload-audio.js',
        'js/jquery.fileupload-video.js',
        'js/jquery.fileupload-validate.js',
        'js/jquery.fileupload-ui.js',

    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'msoft\widgets\fileupload\BlueimpLoadImageAsset',
        'msoft\widgets\fileupload\BlueimpCanvasToBlobAsset',
        'msoft\widgets\fileupload\BlueimpTmplAsset'
    ];
}
