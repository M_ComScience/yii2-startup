<?php
namespace msoft\widgets\chartjs;

use yii\web\AssetBundle;

/**
 *
 * ChartPluginAsset
 */
class ChartJsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/chartjs/dist';

    public $js = [
        'Chart.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
