<?php
namespace msoft\widgets\croppie;

use yii\web\AssetBundle;

class ExifJsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/exif-js';

    public $js = [
        'exif.js'
    ];
}
