<?php

namespace msoft\widgets\tabs;

use msoft\widgets\base\AssetBundle;

class StickyTabsAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/jquery.stickytabs']);
        parent::init();
    }
}
