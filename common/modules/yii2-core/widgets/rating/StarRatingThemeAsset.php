<?php

namespace msoft\widgets\rating;

use msoft\widgets\base\AssetBundle;


class StarRatingThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = __DIR__.'/assets';

    /**
     * @inheritdoc
     */
    public $depends = [
        'msoft\widgets\rating\StarRatingAsset'
    ];
    
    /**
     * Add star rating theme file
     *
     * @param string $theme the theme file name
     */
    public function addTheme($theme) 
    {
        $this->css[] = "css/theme-{$theme}." . (YII_DEBUG ? "css" : "min.css");
    }
}