<?php

namespace msoft\widgets\fileinput;

use msoft\widgets\base\AssetBundle;


class DomPurifyAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__.'/assets/bootstrap-fileinput');
        $this->setupAssets('js', ['js/plugins/purify']);
        parent::init();
    }
}