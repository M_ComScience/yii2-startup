<?php

namespace msoft\widgets\alert;

class AlertAsset extends \msoft\widgets\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/alert']);
        parent::init();
    }
}
