<?php
namespace msoft\widgets\datatables;
use yii\web\AssetBundle;

class DataTablesToolsAsset extends AssetBundle 
{
    public $sourcePath = __DIR__ . '/assets'; 

    public $css = [
        "TableTools/css/dataTables.tableTools.min.css",
    ];

    public $js = [
        "TableTools/js/dataTables.tableTools.min.js",
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'msoft\widgets\datatable\DataTableAsset',
    ];
}