<?php

namespace msoft\widgets\pdfjs;

class Module extends \yii\base\Module
{
    public $buttons = [];
    public $waterMark = false;

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'msoft\widgets\pdfjs\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $waterMarkDefault = [
          'text'=>'',
          'alpha'=>'0.5',
          'color'=>'red'
        ];

        if($this->waterMark!==false && is_array($this->waterMark)){
          $this->waterMark = array_merge($waterMarkDefault,$this->waterMark);
        }else{
          $this->waterMark = $waterMarkDefault;
        }


        $this->buttons = array_merge([
          'presentationMode' => true,
          'openFile' => true,
          'print' => true,
          'download' => true,
          'viewBookmark' => true,
          'secondaryToolbarToggle'=> true
        ], $this->buttons);

        // custom initialization code goes here
    }
}
