<?php

namespace msoft\widgets\builder;

use yii\base\Event;

class ActiveFormEvent extends Event
{
    /**
     * @var string the model attribute name used in the form
     */
    public $attribute;
    /**
     * @var integer the row index of the attribute in the bootstrap grid layout.
     */
    public $index;
    /**
     * @var array any additional event data that can be passed by the event handler as key value pairs.
     */
    public $eventData;
}
