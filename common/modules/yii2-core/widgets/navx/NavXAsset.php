<?php

namespace msoft\widgets\navx;
use msoft\widgets\base\AssetBundle;

class NavXAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/nav-x']);
        parent::init();
    }
}
