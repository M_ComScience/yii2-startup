<?php

namespace msoft\widgets\gallery;

use yii\web\AssetBundle;

class GalleryAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/blueimp-gallery';
    public $css = [
        'css/blueimp-gallery.min.css',
    ];
    public $js = [
        'js/blueimp-gallery.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
