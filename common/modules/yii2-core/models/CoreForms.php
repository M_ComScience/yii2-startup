<?php

namespace msoft\core\models;

use Yii;
use yii\db\ActiveRecord;
use msoft\behaviors\CoreMultiValueBehavior;
use msoft\utils\CoreUtility;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use msoft\widgets\togglecolumn\ToggleActionTrait;
/**
 * This is the model class for table "core_forms".
 *
 * @property integer $form_id
 * @property string $row
 * @property string $action_id
 * @property string $attributes
 * @property string $label
 * @property string $type
 * @property integer $visible
 * @property integer $labelSpan
 * @property string $labelOptions
 * @property string $hint
 * @property string $items
 * @property string $widgetClass
 * @property string $options
 * @property string $columnOptions
 * @property string $columns
 * @property integer $order
 */
class CoreForms extends ActiveRecord implements \msoft\widgets\togglecolumn\ToggleActionInterface
{
    use ToggleActionTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_forms';
    }

    public function behaviors() {
        return [
            [
                'class' => CoreMultiValueBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_AFTER_FIND => ['labelOptions','columnOptions','fieldConfig'],
                ],
                'value' => function ($event) {
                    return CoreUtility::string2strArray($event->sender[$event->data]);
                },
            ],
            [
				'class' => CoreMultiValueBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['labelOptions', 'columnOptions','fieldConfig'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['labelOptions', 'columnOptions','fieldConfig'],
				],
				'value' => function ($event) {
					return CoreUtility::strArray2String($event->sender[$event->data]);
				},
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by','updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_by',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row', 'action_id', 'table_name', 'attributes', 'type'], 'required'],
            [['labelSpan', 'order', 'created_by', 'updated_by','status'], 'integer'],
            [['labelOptions', 'hint', 'items', 'value', 'options', 'fieldConfig', 'columnOptions'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['row', 'action_id', 'table_name', 'attributes', 'label', 'type', 'visible', 'widgetClass', 'columns'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_id' => 'Form ID',
            'row' => 'Row',
            'action_id' => 'Action ID',
            'table_name' => 'Table Name',
            'attributes' => 'Attributes',
            'label' => 'Label',
            'type' => 'Type',
            'visible' => 'Visible',
            'labelSpan' => 'Label Span',
            'labelOptions' => 'Label Options',
            'hint' => 'Hint',
            'items' => 'Items',
            'value' => 'Value',
            'widgetClass' => 'Widget Class',
            'options' => 'Options',
            'fieldConfig' => 'Field Config',
            'columnOptions' => 'Column Options',
            'columns' => 'Columns',
            'order' => 'Order',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return CoreFormsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoreFormsQuery(get_called_class());
    }

    public static function getColumn($tbname,$data) {
        $schema = Yii::$app->db->schema;
        $out = [];
        if(isset($tbname) && $tbname != null){
            $columns = $schema->getTableSchema($tbname)->getColumnNames();
            
            foreach ($columns as $i => $name) {
                $out[$name] = $name;
            }
        }
        
        return $out;
    }

    public function getToggleItems()
	{
	  return  [
		'on' => ['value'=>1, 'label'=>'Show'],
		'off' => ['value'=>0, 'label'=>'Hidden'],
	  ];
	}
}
