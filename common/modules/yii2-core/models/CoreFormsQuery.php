<?php

namespace msoft\core\models;

/**
 * This is the ActiveQuery class for [[CoreForms]].
 *
 * @see CoreForms
 */
class CoreFormsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CoreForms[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CoreForms|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
