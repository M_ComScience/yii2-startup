<?php

namespace msoft\core\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "core_item_alias".
 *
 * @property string $item_code
 * @property string $item_name
 * @property string $item_data
 */
class CoreItemAlias extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */

	public $items;
	public $value;
	public $text;
	public static function tableName() {
		return 'core_item_alias';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['item_code', 'item_name', 'item_data'], 'required'],
			[['item_code'], 'unique'],
			[['item_data'], 'string'],
			[['item_code'], 'string', 'max' => 50],
			[['item_name'], 'string', 'max' => 200]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'item_code' => Yii::t('core', 'ID'),
			'item_name' => Yii::t('core', 'Name'),
			'item_data' => Yii::t('core', 'Data'),
			'items' => 'ข้อมูล'
		];
	}

	public function getItemsData($data){
		$items = [];
		eval("\$item_data = {$data};");
		if(is_array($item_data)){
			foreach($item_data as $key => $value){
				$items[] = ['value' => $key,'text' => $value];
			}
		}
		return $items;
	}

}
