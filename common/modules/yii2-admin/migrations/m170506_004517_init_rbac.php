<?php

use yii\db\Migration;

class m170506_004517_init_rbac extends Migration
{
	private $adminId = 1;

    public function up()
    {
	    $auth = Yii::$app->authManager;

	    $manageUsers = $auth->createPermission('manageUsers');
	    $manageUsers->description = 'Manage users';
	    $auth->add($manageUsers);

	    $user = $auth->createRole('User');
	    $user->description = 'User';
	    $auth->add($user);

	    $Guest = $auth->createRole('Guest');
	    $Guest->description = 'Guest';
	    $auth->add($Guest);

	    $admin = $auth->createRole('Admin');
	    $admin->description = 'Administrator';
	    $auth->add($admin);

	    $admin = $auth->getRole('Admin');
	    $staffPermissions = $auth->getPermissions();

	    // Assign administrator role to admin (1)
		$auth->assign($admin, $this->adminId);
		
		//set data
		$path = __DIR__.'/sql';
        $filenames = $this->dirToArray($path);
        if(is_array($filenames)){
            foreach($filenames as $file){
                if(file_exists($path.'/'.$file)){
                    $this->execute(file_get_contents($path.'/'.$file));
                }
            }
        }
    }

    public function down()
    {
	    Yii::$app->authManager->removeAll();
	}
	
	function dirToArray($dir) { 
        
        $result = array(); 
     
        $cdir = scandir($dir); 
        foreach ($cdir as $key => $value) 
        { 
           if (!in_array($value,array(".",".."))) 
           { 
              if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
              { 
                 $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
              } 
              else 
              { 
                 $result[] = $value; 
              } 
           } 
        } 
        
        return $result; 
    }
}
